package com.higgsup.interestnews.frontend.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class WebController {

    @RequestMapping("/homepage")
    public String homepage(Model model, HttpServletRequest request) {
        model.addAttribute("name", "Value Replace");
        return "home";
    }
}
