package com.higgsup.interestnews.backend.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

public class GsonHelper {
    // Singleton, Factory, Builder, Prototype

    private static GsonHelper instance;

    public static GsonHelper getInstance() {
        if (instance == null)
            instance = new GsonHelper();
        return instance;
    }

    private Gson gson;

    private GsonHelper() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    public Gson getGson() {
        return gson;
    }
}
