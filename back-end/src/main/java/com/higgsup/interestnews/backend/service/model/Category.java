package com.higgsup.interestnews.backend.service.model;

import javax.persistence.*;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue
    @Column(name = "category_id")
    private int categoryID;

    @Column(name = "category_name",nullable = false)
    private String categoryName;

    @Column(name = "category_description",nullable = false)
    private String categoryDes;

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDes() {
        return categoryDes;
    }

    public void setCategoryDes(String categoryDes) {
        this.categoryDes = categoryDes;
    }
}
