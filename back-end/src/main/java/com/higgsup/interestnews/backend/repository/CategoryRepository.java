package com.higgsup.interestnews.backend.repository;

import com.higgsup.interestnews.backend.service.model.Category;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category,Integer>{
    public List<Category> findCategoryIdByCategoryName(String catName);
}
