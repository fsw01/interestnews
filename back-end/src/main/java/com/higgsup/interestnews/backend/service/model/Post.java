package com.higgsup.interestnews.backend.service.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@Entity
@Table(name = "posts")
public class Post {
    @Id
    @Column(name = "post_id",nullable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userID;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category catID;

    @Column(name = "post_title",nullable = false)
    private String postTitle;

    @Column(name = "post_content",nullable = false)
    private String postContent;

    @Column(name = "post_date",nullable = false)
    private Timestamp postDate;

    @Column(name = "post_image",nullable = false)
    private String postImage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUserID() {
        return userID;
    }

    public void setUserID(User userID) {
        this.userID = userID;
    }

    public Category getCatID() {
        return catID;
    }

    public void setCatID(Category catID) {
        this.catID = catID;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Timestamp getPostDate() {
        return postDate;
    }

    public void setPostDate(Timestamp postDate) {
        this.postDate = postDate;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }
}
