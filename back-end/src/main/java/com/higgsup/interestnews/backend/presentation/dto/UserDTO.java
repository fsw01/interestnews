package com.higgsup.interestnews.backend.presentation.dto;

import com.higgsup.interestnews.backend.utils.GsonHelper;

/**
 * Created by lent on 12/24/2015.
 */
public class UserDTO {

    private String username;
    private int roleID;
    private String password;
    private String sessionID;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    @Override
    public String toString() {
        return GsonHelper.getInstance().getGson().toJson(this);
    }
}
