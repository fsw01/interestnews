package com.higgsup.interestnews.backend.exception;

/**
 * Created by Bui Cong Thanh on 12/31/2015.
 */
public class AuthorizationException extends Exception {
    public AuthorizationException(String s) {
        super(s);
    }
}
