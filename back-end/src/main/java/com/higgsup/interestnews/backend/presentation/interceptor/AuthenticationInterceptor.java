package com.higgsup.interestnews.backend.presentation.interceptor;

import com.higgsup.interestnews.backend.exception.AuthorizationException;
import com.higgsup.interestnews.backend.service.impl.UserService;
import com.higgsup.interestnews.backend.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

@Component
public class AuthenticationInterceptor implements HandlerInterceptor {
    @Autowired
    UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        System.out.println("Pre-handle Authentication Interceptor");

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();

        // tìm trong method có khai báo anotation Auth?
        SecureRole roleAnnotation = AnnotationUtils
                .findAnnotation(method, SecureRole.class);

        if (roleAnnotation == null) return true; // Allow by default if no @SecureRole is annotated on the method

        String tokenInput = request.getParameter("token");
        User user = userService.checkToken(tokenInput);
        Role currentRole = Role.valueOf(user.getRole().getRoleName());
        if (roleAnnotation.value().ordinal() > currentRole.ordinal()) {
            throw new AuthorizationException("You don't have required role to acccess the function");
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        System.out.println("Post-handle Authentication");
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        System.out.println("After completion handle Authentication");
    }
}