package com.higgsup.interestnews.backend.presentation.dto;

/**
 * Created by Bui Cong Thanh on 12/30/2015.
 */
public class AuthenticationDTO {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
