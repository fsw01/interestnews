package com.higgsup.interestnews.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlingController {

    // Convert a predefined exception to an HTTP Status code
    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "You don't have token to access")  // 403
    @ExceptionHandler(AuthenticationException.class)
    @ResponseBody
    public void conflict(HttpServletResponse response) throws IOException {
        // Nothing to do
        response.getWriter().append("Something happen");
    }
}