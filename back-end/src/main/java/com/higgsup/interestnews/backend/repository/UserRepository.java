package com.higgsup.interestnews.backend.repository;

import com.higgsup.interestnews.backend.service.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>, ICustomUserRepository {
    public List<User> findByUserName(String userName);
    public List<User> findBySessionID(String userName);
    public List<User> findUserIdByUserName(String userName);
}
