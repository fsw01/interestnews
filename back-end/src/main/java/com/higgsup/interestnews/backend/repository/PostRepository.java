package com.higgsup.interestnews.backend.repository;

import com.higgsup.interestnews.backend.service.model.Post;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@Repository
public interface PostRepository extends PagingAndSortingRepository<Post,Integer>{
    public List<Post> findPostByUserId(int userID);
}
