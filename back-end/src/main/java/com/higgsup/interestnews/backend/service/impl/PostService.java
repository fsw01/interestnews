package com.higgsup.interestnews.backend.service.impl;

import com.higgsup.interestnews.backend.repository.CategoryRepository;
import com.higgsup.interestnews.backend.repository.PostRepository;
import com.higgsup.interestnews.backend.repository.UserRepository;
import com.higgsup.interestnews.backend.service.IPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@Service
@Transactional(readOnly = false)
public class PostService implements IPostService{
    @Autowired
    PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

//    public Post createPost(PostDTO postDTO){
//        Post post1 = new Post();
//        post1.setUserID(userRepository.findUserIdByUserName(postDTO.getUserName()));
//        post1.setCatID(categoryRepository.findCategoryIdByCategoryName(postDTO.getCatName()));
//        post1.setPostTitle(postDTO.getPostTitle());
//        post1.setPostContent(postDTO.getPostContent());
//        post1.setPostDate(postDTO.getPostDate());
//        post1.setPostImage(post1.getPostImage());
//        return postRepository.save(post1);
//
//    }
}
