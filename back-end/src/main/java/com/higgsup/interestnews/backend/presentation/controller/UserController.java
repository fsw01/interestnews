package com.higgsup.interestnews.backend.presentation.controller;

import com.higgsup.interestnews.backend.presentation.dto.AuthenticationDTO;
import com.higgsup.interestnews.backend.presentation.dto.UserDTO;
import com.higgsup.interestnews.backend.service.impl.UserService;
import com.higgsup.interestnews.backend.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(path = "/createUser",method = RequestMethod.POST)
    @ResponseBody
    public User createUser(@RequestBody UserDTO user) throws Exception {
        return userService.createUser(user);
    }

    @RequestMapping(path = "/login",method = RequestMethod.POST)
    @ResponseBody
    public AuthenticationDTO login(@RequestBody UserDTO user) throws Exception {
        return userService.login(user);
    }

    @RequestMapping(path = "/checkAuthentication", method = RequestMethod.POST,produces = "application/text")
    @ResponseBody
    public User checkAuthentication(HttpServletRequest request) throws Exception {
        return userService.checkToken(request.getParameter("token"));
    }


}
