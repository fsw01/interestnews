package com.higgsup.interestnews.backend.presentation.controller;

import com.higgsup.interestnews.backend.presentation.interceptor.Role;
import com.higgsup.interestnews.backend.presentation.interceptor.SecureRole;
import com.higgsup.interestnews.backend.service.impl.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bui Cong Thanh on 1/5/2016.
 */
@CrossOrigin
@RestController
public class PostController {
    @Autowired
    PostService postService;

//    @RequestMapping(path = "/createPost",method = RequestMethod.POST)
//    @ResponseBody
//    @SecureRole(Role.AUTHOR)
//    public Post createPost(@RequestBody PostDTO postDTO) throws Exception {
//        return postService.createPost(postDTO);
//    }

    @RequestMapping(path = "/viewPost", method = RequestMethod.POST, produces = "application/text", consumes = "application/text")
    @ResponseBody
    @SecureRole(Role.ADMIN)
    public String viewPost() throws Exception {
        return "You view this post";
    }

    @RequestMapping(path = "/editPost",method = RequestMethod.POST, produces = "application/text")
    @ResponseBody
    @SecureRole(Role.CONTRIBUTOR)
    public String editPost() throws Exception {
        return "You have permission to edit post";
    }

    @RequestMapping(path = "/publishPost",method = RequestMethod.POST, produces = "application/text")
    @ResponseBody
    @SecureRole(Role.EDITOR)
    public String publishPost() throws Exception {
        return "You have permission to publish post";
    }
}
