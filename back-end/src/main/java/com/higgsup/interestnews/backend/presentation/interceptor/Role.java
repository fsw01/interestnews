package com.higgsup.interestnews.backend.presentation.interceptor;

/**
 * Created by Bui Cong Thanh on 12/31/2015.
 */
public enum Role {
    SUBCRIBER,
    CONTRIBUTOR,
    EDITOR,
    ADMIN
}
