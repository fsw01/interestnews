package com.higgsup.interestnews.backend;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StandardConfiguration {
    @Bean
    public ApplicationInfo applicationInfo() {
        return new ApplicationInfo();
    }

}
