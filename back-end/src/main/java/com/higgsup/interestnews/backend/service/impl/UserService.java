package com.higgsup.interestnews.backend.service.impl;

import com.higgsup.interestnews.backend.presentation.dto.AuthenticationDTO;
import com.higgsup.interestnews.backend.presentation.dto.UserDTO;
import com.higgsup.interestnews.backend.repository.RoleRepository;
import com.higgsup.interestnews.backend.repository.UserRepository;
import com.higgsup.interestnews.backend.service.IUserService;
import com.higgsup.interestnews.backend.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@Transactional(readOnly = true)//k cho ghi du lieu vao db
public class UserService implements IUserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Transactional(readOnly = false)
    public User createUser(UserDTO user){
        User user1 = new User();
        user1.setRole(roleRepository.findOne(5));
        user1.setUserName(user.getUsername());
        user1.setPassword(user.getPassword());

        return userRepository.save(user1);


    }

    @Transactional(readOnly = false)//cho phep ghi
    public AuthenticationDTO login(UserDTO user) throws Exception {
        List<User> result = userRepository.findByUserName(user.getUsername());
        if(result.size() == 0){
            throw new Exception("user doesn't exits!");
        }

        User user1 = result.get(0);


        if(!user1.getPassword().equals(user.getPassword())){
            throw new Exception("Password is wrong !");
        }


        user1.setSessionID(UUID.randomUUID().toString());
        userRepository.save(user1);

        AuthenticationDTO authenticationDTO = new AuthenticationDTO();
        authenticationDTO.setToken(user1.getSessionID());

        return authenticationDTO;
    }

    public User checkToken(String token) throws Exception {
        List<User> result = userRepository.findBySessionID(token);
        // Return a user (Role)
        return result.get(0);
    }

}
